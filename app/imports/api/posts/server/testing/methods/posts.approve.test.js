import {assert} from 'chai';
import sinon from 'sinon';
import methods from '/imports/api/posts/server/methods';
import PostService from '/imports/core/services/posts/PostService';
import {restoreIfStubbed, userId, adminId, createPost} from '/imports/core/helpers/testHelper';

describe('Methods', function() {
    describe('Approve Post', function() {
        afterEach(function() {
            restoreIfStubbed(PostService.approvePost);
        });

        it('Should allow administrators approve a post', function() {
            sinon.stub(PostService, 'approvePost');

            const postId = createPost();

            methods['posts.approve'].call({userId: adminId}, postId);

            assert.isTrue(PostService.approvePost.calledWith(postId));
        });

        it('Should prevent unauthenticated users from approving a post', function() {
            sinon.stub(PostService, 'approvePost');

            const errorType = 'not-authorized';
            const errorReason = 'You are not authorized.';

            const postId = createPost();

            assert.throws(function() {
                methods['posts.approve'].call({}, postId);
            }, errorType, errorReason);

            assert.isTrue(PostService.approvePost.notCalled);
        });

        it('Should prevent users that are not administrators from approving a post', function() {
            sinon.stub(PostService, 'approvePost');

            const postId = createPost();

            assert.throws(function() {
                methods['posts.approve'].call({userId}, postId);
            }, /not-authorized/);

            assert.isTrue(PostService.approvePost.notCalled);
        });

        it('Should prevent post approval when request data is invalid', function() {
            sinon.stub(PostService, 'approvePost');

            assert.throws(function() {
                methods['posts.approve'].call({userId: adminId}, undefined);
            }, /postId is required/);

            assert.isTrue(PostService.approvePost.notCalled);
        });

        it('Should validate the presence of the `postId` field', function() {
            assert.throws(function() {
                methods['posts.approve'].call({userId: adminId}, undefined);
            }, /postId is required/);
        });
    });     
});