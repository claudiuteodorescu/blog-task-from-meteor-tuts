import {assert} from 'chai';
import sinon from 'sinon';
import methods from '/imports/api/posts/server/methods';
import PostService from '/imports/core/services/posts/PostService';
import {userId, getPostDataSample, restoreIfStubbed} from '/imports/core/helpers/testHelper';

describe('Methods', function() {
    describe('Create Post', function() {
        afterEach(function() {
            restoreIfStubbed(PostService.createPost);
        });

        it('Should be able to create a post', function() {
            sinon.stub(PostService, 'createPost');

            methods['posts.create'].call({userId}, getPostDataSample());

            assert.isTrue(PostService.createPost.calledWith(userId, getPostDataSample()));
        });

        it('Should create only one post', function() {
            sinon.stub(PostService, 'createPost');

            methods['posts.create'].call({userId}, getPostDataSample());

            assert.isTrue(PostService.createPost.calledOnce);
        });

        it('Should prevent unauthenticated users from creating a post', function() {
            sinon.stub(PostService, 'createPost');

            const errorType = 'not-authorized';
            const errorReason = 'You are not authorized.';

            assert.throws(function() {
                methods['posts.create'].call({}, getPostDataSample());
            }, errorType, errorReason);

            assert.isTrue(PostService.createPost.notCalled);
        });

        describe('Validation', function() {
            it('Should prevent post creation', function() {
                sinon.stub(PostService, 'createPost');

                assert.throws(function() {
                    methods['posts.create'].call({userId}, {description: 'test description'});
                }, /Title is required/);

                assert.isTrue(PostService.createPost.notCalled);
            });

            it('Should make extra fields are removed before post creation', function() {
                sinon.stub(PostService, 'createPost');

                const newPostDataSample = {extraField: 'I am extra', title: 'A title', description: 'A description'};
                const cleanedPostDataSample = _.clone(newPostDataSample);
                delete cleanedPostDataSample.extraField;

                methods['posts.create'].call({userId}, newPostDataSample);

                assert.isTrue(PostService.createPost.calledWith(userId, cleanedPostDataSample));
            });

            describe('Title Validation', function() {
                it('Should validate the presence of the post title', function() {
                    assert.throws(function() {
                        methods['posts.create'].call({userId}, {description: 'test description'});
                    }, /Title is required/);
                });

                it('Should not allow blank post title', function() {
                    assert.throws(function() {
                        methods['posts.create'].call({userId}, {title: ' ', description: 'test description'});
                    }, /Title is required/);
                });
            });

            describe('Description Validation', function() {
                it('Should validate the presence of post description', function() {
                    assert.throws(function() {
                        methods['posts.create'].call({userId}, {title: 'Title'});
                    }, /Description is required/);
                });

                it('Should not allow blank post description', function() {
                    assert.throws(function() {
                        methods['posts.create'].call({userId}, {title: 'Title', 'description': ' '});
                    }, /Description is required/);
                });
            });
        });
    });
});