import {Meteor} from 'meteor/meteor';
import PostService from '/imports/core/services/posts/PostService';
import {Users} from '/imports/core/db';
import Security from '/imports/core/security';
import SimpleSchema from 'simpl-schema';
import getPostTagsValidationRules from '/imports/core/services/validation/getPostTagsValidationRules';

const methods = {
    'posts.create': function(postData) {
        const schema = new SimpleSchema({
            title: {type: String, max: 200},
            description: {type: String},
            ...getPostTagsValidationRules(),
        });

        const cleanPostData = schema.clean(postData);

        schema.validate(cleanPostData);

        Security.checkLoggedIn(this.userId);

        PostService.createPost(this.userId, cleanPostData);
    },

    'posts.approve': function(postId) {
        const inputData = {postId};

        const schema = new SimpleSchema({
            postId: {type: String, label: 'postId'},
        });

        const cleanInputData = schema.clean(inputData);

        schema.validate(cleanInputData);

        Security.checkLoggedIn(this.userId);

        Security.checkRole(this.userId, 'admin');

        PostService.approvePost(cleanInputData.postId);
    },

    'profile.update': function({user}) {
        const schema = new SimpleSchema({
            ...getPostTagsValidationRules('interests'),
        });

        const cleanUserInput = schema.clean(user);
        schema.validate(cleanUserInput);

        Security.checkLoggedIn(this.userId);

        Users.update(this.userId, {$set: cleanUserInput})
    }
}

Meteor.methods(methods);

export default methods;