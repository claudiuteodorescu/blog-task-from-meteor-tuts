import {getPendingPostQuery} from '/imports/core/db/queries';
import Security from '/imports/core/security';
import RolesEnum from '/imports/core/db/users/enums/roles';

getPendingPostQuery.expose({
    firewall(userId) {
        Security.checkLoggedIn(userId);

        Security.checkRole(userId, RolesEnum.ADMIN);
    }
});