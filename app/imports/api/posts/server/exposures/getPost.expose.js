import {getPostQuery} from '/imports/core/db/queries';
import Security from '/imports/core/security';
import RolesEnum from '/imports/core/db/users/enums/roles';

getPostQuery.expose({
    firewall(userId, params) {
        params.userId = userId;
    },

    embody: {
        $filter({filters, params}) {
            if (params.postId) {
                filters._id = params.postId;
            }
            
            // by default, the public can query only approved posts
            filters.approved = true;

            const isAdmin = params.userId && Security.hasRole(params.userId, RolesEnum.ADMIN);

            // if the user is admin, give them all posts
            if (isAdmin) {
                delete filters.approved;
            }
        }
    }
});