import './exposures/getPendingPost.expose';
import './exposures/getApprovedPost.expose';
import './exposures/getPost.expose';
import './methods';