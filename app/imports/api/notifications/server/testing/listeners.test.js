import {assert} from 'chai';
import sinon from 'sinon';
import NotificationService from '/imports/core/services/notifications/NotificationService';
import {Emitter, EventsEnum} from '/imports/core/events';
import {createPost} from '/imports/core/helpers/testHelper';
import '/imports/api/notifications/server/listeners';

let postId;

describe('Notification Listeners', function() {
    beforeEach(function() {
        postId = createPost();
    });

    it('Should notify admins for approval when a post is created', function() {
        sinon.stub(NotificationService, 'notifyAdminsForPostApproval');

        Emitter.emit(EventsEnum.POST_CREATED, {postId});

        assert.isTrue(
            NotificationService.notifyAdminsForPostApproval.calledWith(postId)
        );

        NotificationService.notifyAdminsForPostApproval.restore();
    });

    it('Should notify interested users when a post was approved', function() {
        sinon.stub(NotificationService, 'notifyUsersInterestedOfPost');

        Emitter.emit(EventsEnum.POST_APPROVED, {postId});
        
        assert.isTrue(
            NotificationService.notifyUsersInterestedOfPost.calledWith(postId)
        );

        NotificationService.notifyUsersInterestedOfPost.restore();
    });
})