import NotificationService from '/imports/core/services/notifications/NotificationService';
import {Emitter, EventsEnum} from '/imports/core/events';

Emitter.on(EventsEnum.POST_CREATED, function({postId}) {
    NotificationService.notifyAdminsForPostApproval(postId);
});

Emitter.on(EventsEnum.POST_APPROVED, function({postId}) {
    NotificationService.notifyUsersInterestedOfPost(postId);
});