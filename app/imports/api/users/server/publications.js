import {Meteor} from 'meteor/meteor';

Meteor.publish('userData', function() {
    if(!this.userId) return null;

    return Meteor.users.find(
        this.userId,
        {fields: {interests: 1}}
    );
});