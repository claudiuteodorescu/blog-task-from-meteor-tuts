import React from 'react';
import PropTypes from 'prop-types';
import PostListItem from '/imports/client/ui/PostListItem';
import {withTracker} from 'meteor/react-meteor-data';
import getApprovedPostQuery from '/imports/core/db/posts/queries/getApprovedPost';
import getPendingPostQuery from '/imports/core/db/posts/queries/getPendingPost';

const PostList = ({posts}) => {
    posts = posts || [];

    return (
        <div>
        {
            posts.map(post => <PostListItem key={post._id} post={post} />)
        }
        </div>
    );
};

PostList.propTypes = {
    posts: PropTypes.array
}

export default withTracker(({approved}) => {
    let query;

    // TODO: get only the `_id` and `title` fields.
    if (approved) {
        query = getApprovedPostQuery.clone();
    }
    else {
        query = getPendingPostQuery.clone();
    }

    query.subscribe();

    return {
        posts: query.fetch()
    };
})(PostList);