import React from 'react';
import PropTypes from 'prop-types';
import getPostQuery from '/imports/core/db/posts/queries/getPost';
import {PostTagsLabels} from '/imports/core/db/posts/enums/tags';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';

class Post extends React.Component {
    constructor(props) {
        super(props);

        this.onApprovePost = this.onApprovePost.bind(this);
    }

    onApprovePost(event) {
        event.preventDefault();

        if (!confirm('Are you sure you approve this post?')) {
            return;
        }

        Meteor.call('posts.approve', this.props.post._id, (err) => {
            if (err) {
                return alert(err);
            }

            alert('Post approved succesfully.');
        });
    }

    render() {
        const post = this.props.post;

        if (!post) {
            return null;
        }

        const postTags = (post.tags || []).map(tag => PostTagsLabels[tag]);

        return (
            <div className="blgt-post">
                <h1>{post.title}</h1>

                {
                    postTags.length > 0 ? <div>Tags: <em>{postTags.join(', ')}</em></div> : null
                }

                <p className="blgt-post__description">{post.description}</p>

                {
                    post.approved
                    ?
                    null
                    :
                    <p><a href='' onClick={this.onApprovePost}>Approve</a></p>
                }
            </div>
        );
    }
};

Post.propTypes = {
    post: PropTypes.object
}

export default withTracker(({postId}) => {
    const currentPostQuery = getPostQuery.clone({postId});
    currentPostQuery.subscribe();

    return {
        post: currentPostQuery.fetchOne()
    };
})(Post);