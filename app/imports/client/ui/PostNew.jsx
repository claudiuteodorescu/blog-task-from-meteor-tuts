import React from 'react';
import PropTypes from 'prop-types';
import {PostTagsLabels} from '/imports/core/db/posts/enums/tags';
import {Meteor} from 'meteor/meteor';

class PostNew extends React.Component {
    constructor(props) {
        super(props);

        this.state = {formData: {}};

        this.onSubmit = this.onSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.onMultipleSelectChange = this.onMultipleSelectChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.formIsValid()) {
            return alert('Please fill all the fields');
        }

        const formData = this.state.formData;

        Meteor.call('posts.create', formData, (err) => {
            if (err) {
                return alert(err);
            }
            
            alert('Post succesfully created.\nIt will be posted after an administrator will approve it.');

            if (this.props.onSuccess) {
                this.props.onSuccess();
            }
        });
    }

    formIsValid() {
        const title = this.getFormFieldValue('title');
        const description = this.getFormFieldValue('description');

        return title && description;
    }

    getFormFieldValue(name) {
        return (this.state.formData[name] || "").trim();
    }

    onChange({target: element}) {
        const {name, value} = element;

        const formData = this.state.formData;

        const newFormData = Object.assign({}, formData, {
            [name]: value.trim()
        });

        this.setState({
            formData: newFormData
        });
    }

    onMultipleSelectChange({target: element}) {
        const tags = Array.from(element.options)
            .filter(option => option.selected)
            .map(option => option.value);

        const newFormData = Object.assign(
            {},
            this.state.formData,
            {[element.name]: tags}
        );

        this.setState({
            formData: newFormData
        });
    }

    render() {
        const {title, description, tags} = this.state.formData;

        return (
            <div>
                <h1>Create Post</h1>

                <form onSubmit={this.onSubmit}>
                    <h2>Title</h2>
                    <p>
                        <input
                            defaultValue={title}
                            type="text"
                            name="title"
                            onChange={this.onChange}
                        />
                    </p>

                    <h2>Description</h2>
                    <p>
                        <textarea
                            defaultValue={description}
                            name="description"
                            onChange={this.onChange}
                        ></textarea>
                    </p>

                    <h2>Interests</h2>
                    <p>
                        <select
                            defaultValue={tags}
                            name="tags"
                            onChange={this.onMultipleSelectChange}
                            multiple
                        >
                        {
                            Object.keys(PostTagsLabels).map(tag => {
                                return (
                                    <option key={tag} value={tag}>
                                        {PostTagsLabels[tag]}
                                    </option>
                                );
                            })
                        }
                        </select>
                    </p>

                    <p>
                        <input type="submit" />
                    </p>
                </form>
            </div>
        );
    }
};

PostNew.propTypes = {
    onSuccess: PropTypes.func,
}

export default PostNew;