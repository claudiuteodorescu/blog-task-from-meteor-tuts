import React from 'react';
import PropTypes from 'prop-types';
import RouteService from '/imports/core/services/routes/RouteService';

const PostListItem = ({post}) => {
    const postUrl = RouteService.postUrl(post._id);

    return (
        <div>
            <a href={postUrl}>
                {post.title}
            </a>
        </div>
    );
};

PostListItem.propTypes = {
    post: PropTypes.object.isRequired
}

export default PostListItem;