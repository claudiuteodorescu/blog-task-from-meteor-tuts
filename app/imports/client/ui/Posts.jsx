import React from 'react';
import PropTypes from 'prop-types';
import PostList from '/imports/client/ui/PostList';

const Posts = ({approved}) => {
    return (
        <div>
            <h1>Posts</h1>

            <PostList approved={approved} />
        </div>
    );
};

Posts.propTypes = {
    approved: PropTypes.bool,
};

export default Posts;