import React from 'react';
import PropTypes from 'prop-types';
import {PostTagsLabels} from '/imports/core/db/posts/enums/tags';
import {Meteor} from 'meteor/meteor';
import {withTracker} from 'meteor/react-meteor-data';

class UserProfile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {userData: {}};
        this.onSubmit = this.onSubmit.bind(this);
        this.onMultipleSelectChange = this.onMultipleSelectChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.ready || !nextProps.user) {
            return;
        }

        this.setState({
            userData: {
                interests: nextProps.user.interests
            }
        });
    }

    onSubmit(event) {
        event.preventDefault();

        const user = this.state.userData;

        Meteor.call('profile.update', {user}, (err) => {
            if (err) {
                return alert(err);
            }

            alert('Profile updated succesfully.');
        });
    }

    onMultipleSelectChange({target: element}) {
        const interests = Array.from(element.options)
            .filter(option => option.selected)
            .map(option => option.value);

        const newFormData = Object.assign(
            {},
            this.state.userData,
            {[element.name]: interests}
        );

        this.setState({
            userData: newFormData
        });
    }

    render() {
        if (!this.props.ready) {
            return null;
        }

        const interests = this.state.userData.interests;
        const name = this.props.user.profile.name;

        return (
            <div>
                <h1>My Profile - {name}</h1>

                <h2>Interests</h2>
                <form onSubmit={this.onSubmit}>
                    <p>
                        <select
                            name="interests"
                            defaultValue={interests}
                            onChange={this.onMultipleSelectChange}
                            multiple={true}
                        >
                        {
                            Object.keys(PostTagsLabels).map(tag => {
                                return (
                                    <option key={tag} value={tag}>
                                        {PostTagsLabels[tag]}
                                    </option>
                                );
                            })
                        }
                        </select>
                    </p>

                    <p>
                        <input type="submit" value="Update" />
                    </p>
                </form>
            </div>
        );
    }
};

UserProfile.propTypes = {
    ready: PropTypes.bool,
    user: PropTypes.object,
}

export default withTracker(() => {
    const handle = Meteor.subscribe('userData');

    return {
        ready: handle.ready(),
        user: Meteor.users.findOne(Meteor.userId())
    };
})(UserProfile);