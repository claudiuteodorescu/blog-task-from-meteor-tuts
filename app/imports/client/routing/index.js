import route from '/imports/client/routing/router';
import Posts from '../ui/Posts.jsx';
import Post from '../ui/Post.jsx';
import PostNew from '../ui/PostNew.jsx';
import UserProfile from '../ui/UserProfile.jsx';
import RouteService from '/imports/core/services/routes/RouteService';

route('/', null, {}, {
    triggersEnter: [function(context, redirect) {
        redirect(RouteService.postsPath());
    }]
});

route(RouteService.postsPath(), Posts, {approved: true});

route(RouteService.pendingPostsPath(), Posts);

route(RouteService.newPostPath(), PostNew, {
    onSuccess: () => route.go(RouteService.postsPath())
});

route(RouteService.postPath(':postId'), Post);

route(RouteService.userProfilePath(), UserProfile);