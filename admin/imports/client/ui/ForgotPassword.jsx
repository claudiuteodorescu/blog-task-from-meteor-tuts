import React from 'react';
import PropTypes from 'prop-types';
import {Accounts} from 'meteor/accounts-base';
import RouteService from '/imports/core/services/routes/RouteService';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.onSubmit = this.onSubmit.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();
        
        const email = this.state.email;
        if (!email) {
            return alert('Please enter an email.');
        }

        Accounts.forgotPassword({email}, (err) => {
            if (err) {
                return alert(err);
            }

            alert('Please check your email for password reset instructions.');

            if (this.props.onSuccess) {
                this.props.onSuccess();
            }
        });
    }

    onEmailChange(event) {
        const email = event.target.value.trim();
        this.setState({email});
    }

    render() {
        return (
            <div>
                <h1>Forgot Password</h1>
                <div>
                    Please enter your email
                </div>
                <form onSubmit={this.onSubmit}>
                    <input type="email" onChange={this.onEmailChange} />
                    <input type="submit" value="Submit" />
                </form>
                <br />
                Go to <a href={RouteService.loginUrl()}>Login</a>.
            </div>
        );
    }
};


ForgotPassword.propTypes = {
    onSuccess: PropTypes.func,
}
export default ForgotPassword;