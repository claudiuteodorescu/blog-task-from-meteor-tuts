import React from 'react';
import PropTypes from 'prop-types';
import {Accounts} from 'meteor/accounts-base';
import AuthenticationForm from '/imports/client/ui/AuthenticationForm.jsx';
import RouteService from '/imports/core/services/routes/RouteService';

class Register extends React.Component {
    constructor(props) {
        super(props);

        this.onValidSubmit = this.onValidSubmit.bind(this);
    }

    onValidSubmit({email, password}) {
        Accounts.createUser({email, password}, (err) => {
            if (err) {
                return alert(err);
            }

            if (this.props.onSuccess) {
                this.props.onSuccess();
            }
        });
    };

    render() {
        return (
            <div>
                <h1>Register</h1>
                <AuthenticationForm
                    submitLabel="Register"
                    onValidSubmit={this.onValidSubmit}
                />
                <br />
                <div>
                    {"Already have an account?"}&nbsp;
                    <a href={RouteService.loginUrl()}>Login!</a>
                </div>
            </div>
        );
    }
}

Register.propTypes = {
    onSuccess: PropTypes.func,
};

export default Register;