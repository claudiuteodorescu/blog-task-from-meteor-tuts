import React from 'react';
import PropTypes from 'prop-types';
import {Meteor} from 'meteor/meteor';
import AuthenticationForm from '/imports/client/ui/AuthenticationForm.jsx';
import RouteService from '/imports/core/services/routes/RouteService';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.onValidSubmit = this.onValidSubmit.bind(this);
    }

    onValidSubmit({email, password}) {
        Meteor.loginWithPassword(email, password, (err) => {
            if (err) {
                return alert(err);
            }

            if (this.props.onSuccess) {
                this.props.onSuccess();
            }
        });
    }

    render() {
        return (
            <div>
                <h1>Login</h1>
                <AuthenticationForm
                    submitLabel="Login"
                    onValidSubmit={this.onValidSubmit}
                />
                <br />
                <div>
                    <a href={RouteService.forgotPasswordUrl()}>Forgot password?</a>
                </div>
            </div>
        );
    }
}

Login.propTypes = {
    onSuccess: PropTypes.func,
};

export default Login;