import React from 'react';
import PropTypes from 'prop-types';
import {Accounts} from 'meteor/accounts-base';
import RouteService from '/imports/core/services/routes/RouteService';

class ResetPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.onSubmit = this.onSubmit.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        const token = this.props.token;
        const password = this.state.password;
        if (!password) {
            return alert('Please enter a password.');
        }

        Accounts.resetPassword(token, password, (err) => {
            if (err) {
                return alert(err);
            }

            alert('Your password was reset succesfully.');

            if (this.props.onSuccess) {
                this.props.onSuccess();
            }
        });
    }

    onPasswordChange(event) {
        const password = event.target.value.trim();
        this.setState({password});
    }

    render() {
        return (
            <div>
                <h1>Reset Password</h1>
                <div>
                    Please enter a new password
                </div>
                <form onSubmit={this.onSubmit}>
                    <input type="password" onChange={this.onPasswordChange} />
                    <input type="submit" value="Submit" />
                </form>
                <br />
                Go to <a href={RouteService.loginUrl()}>Login</a>.
            </div>
        );
    }
};


ResetPassword.propTypes = {
    token: PropTypes.string,
    onSuccess: PropTypes.func,
}
export default ResetPassword;