import React from 'react';
import PropTypes from 'prop-types';

class AuthenticationForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.onFieldChange = this.onFieldChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.formIsValid()) {
            return alert('Please fill all the fields');
        }

        if (this.props.onValidSubmit) {
            const email = this.getFormFieldValue('email');
            const password = this.getFormFieldValue('password');

            this.props.onValidSubmit({email, password});
        }
    }

    formIsValid() {
        const email = this.getFormFieldValue('email');
        const password = this.getFormFieldValue('password');

        return email && password;
    }

    getFormFieldValue(name) {
        return (this.state[name] || "").trim();
    }

    onFieldChange(event) {
        this.setState({[event.target.name]: event.target.value});
    }

    render() {
        const {email, password} = this.state;
        const submitLabel = this.props.submitLabel || 'Submit';

        return (
            <form onSubmit={this.onSubmit}>
                <div>
                    <label htmlFor="login-form-email">Email:</label>
                    <input
                        id="login-form-email"
                        name="email"
                        type="text"
                        defaultValue={email}
                        onChange={this.onFieldChange}
                    />
                </div>

                <div>
                    <label htmlFor="login-form-password">Password:</label>
                    <input
                        id="login-form-password"
                        name="password"
                        type="password"
                        defaultValue={password}
                        onChange={this.onFieldChange}
                    />
                </div>

                <input type="submit" value={submitLabel} />
            </form>
        );
    }
};

AuthenticationForm.propTypes = {
    submitLabel: PropTypes.string,
    onValidSubmit: PropTypes.func,
};

export default AuthenticationForm;