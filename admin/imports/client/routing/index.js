import route from '/imports/client/routing/router';
import {redirectToPostsIfLoggedIn, redirectToPosts, performLogout} from '/imports/client/routing/triggers';
import RouteService from '/imports/core/services/routes/RouteService';
import Login from '/imports/client/ui/Login.jsx';
import Register from '/imports/client/ui/Register.jsx';
import ForgotPassword from '/imports/client/ui/ForgotPassword.jsx';
import ResetPassword from '/imports/client/ui/ResetPassword.jsx';

route(RouteService.loginPath(), Login, {
    onSuccess: redirectToPosts,
}, {
    triggersEnter: [redirectToPostsIfLoggedIn]
});

route(RouteService.registerPath(), Register, {
    onSuccess: redirectToPosts,
}, {
    triggersEnter: [redirectToPostsIfLoggedIn]
});

route(RouteService.forgotPasswordPath(), ForgotPassword, {
    onSuccess: () => route.go(RouteService.loginPath()),
}, {
    triggersEnter: [redirectToPostsIfLoggedIn]
});

route(RouteService.resetPasswordPath(':token'), ResetPassword, {
    onSuccess: redirectToPosts,
}, {
    triggersEnter: [redirectToPostsIfLoggedIn]
});

route(RouteService.logoutPath(), null, {}, {
    triggersEnter: [performLogout]
});