import { createRouter } from 'meteor/cultofcoders:meteor-react-routing';
import App from '/imports/core/client/ui/App.jsx';

export default createRouter(App);