import RouteService from '/imports/core/services/routes/RouteService';
import {Meteor} from 'meteor/meteor';

const redirectToPosts = () => {
    window.location.href = RouteService.postsUrl();
};

const redirectToPostsIfLoggedIn = function() {
    if (Meteor.userId()) {
        redirectToPosts();
    }
};

const performLogout = function() {
    Meteor.logout(() => window.location.href = RouteService.loginUrl());
};

export {
    redirectToPostsIfLoggedIn,
    redirectToPosts,
    performLogout,
};