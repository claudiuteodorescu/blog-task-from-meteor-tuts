import renderReactComponentToHtml from '/imports/core/services/rendering/renderReactComponentToHtml';
import ResetPassword from '/imports/core/emails/templates/ResetPassword.jsx';
import {Accounts} from 'meteor/accounts-base';

Accounts.emailTemplates.resetPassword.text = (user) => {
    const token = user.services.password.reset.token;
    
    return renderReactComponentToHtml(ResetPassword, {token});
}