# Beat the boss - Meteor Tuts End Task

This is my approach to the task given at the end of these great Meteor & programming tutorials: [http://www.meteor-tuts.com/](http://www.meteor-tuts.com/).

The task, in short:

1. User creates a post => the admins will be notified for that post
2. Admin reviews post and eventually approves it => the users interested in the post's topics will be notified about the post

So this is basically a small blog app.