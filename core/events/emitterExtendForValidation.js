import {Events} from './events';

export default (emitter) => {
    const oldEmit = emitter.emit.bind(emitter);
    const newEmit = function(event, data) {
        if (!Events[event]) {
            return;
        }

        if (Events[event].validate) {
            Events[event].validate(data);
        }

        oldEmit(event, data);
    };

    emitter.emit = newEmit.bind(emitter);

    return emitter;
}