import EventEmitter from 'event-emitter';

import emitterExtendForValidation from './emitterExtendForValidation';

const Emitter = emitterExtendForValidation(EventEmitter());

export default Emitter;

export {Emitter};
export * from './events';