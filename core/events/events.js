import withValidation from './withValidation';

const EventsEnum = {
    POST_CREATED: 'post_created',
    POST_APPROVED: 'post_approved',
};

let Events = {
    [EventsEnum.POST_CREATED]: {
        schema: {
            postId: {type: String},
        }
    },

    [EventsEnum.POST_APPROVED]: {
        schema: {
            postId: {type: String},
        }
    }
};

Events = withValidation(Events);

export default Events;

export {Events, EventsEnum};