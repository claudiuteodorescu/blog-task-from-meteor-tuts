import SimpleSchema from 'simpl-schema';

const withValidation = (Events) => {
    _.each(Events, (value, key) => {
        if (!Events[key].schema) {
            return;
        }

        Events[key].validate = (new SimpleSchema(Events[key].schema)).validator();
    })

    return Events;
}

export default withValidation;