import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../layouts/Layout.jsx';
import RouteService from '../../services/routes/RouteService';

const ApprovePostRequest = ({post}) => {
    const postUrl = RouteService.postUrl(post._id);

    return (
        <Layout header="Post Approval Request">
            To review the post, click here: <a href={postUrl}>{postUrl}</a>.
        </Layout>
    );
};

ApprovePostRequest.propTypes = {
    post: PropTypes.shape({
        _id: PropTypes.string.isRequired,
    }),
};

export default ApprovePostRequest;