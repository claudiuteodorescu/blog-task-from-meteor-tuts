import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../layouts/Layout.jsx';
import RouteService from '../../services/routes/RouteService';

const ResetPassword = ({token}) => {
    const resetUrl = RouteService.resetPasswordUrl(token);

    return (
        <Layout header="Reset Password">
            To reset your password for your Blog Task account, please visit this url: 
            <a href={resetUrl}>{resetUrl}</a>
            .
        </Layout>
    );
};

ResetPassword.propTypes = {
    resetUrl: PropTypes.string.isRequired,
};

export default ResetPassword;