import React from 'react';
import PropTypes from 'prop-types';
import Layout from '../layouts/Layout.jsx';
import RouteService from '../../services/routes/RouteService';

const NewPostOfInterest = ({post}) => {
    const postUrl = RouteService.postUrl(post._id);

    return (
        <Layout header="Post of interest created">
            A new post that you might be interested in, was created: {post.title}.
            To read it click here: <a href={postUrl}>{postUrl}</a>.
        </Layout>
    );
};

NewPostOfInterest.propTypes = {
    post: PropTypes.shape({
        _id: PropTypes.string.isRequired,
    }),
};

export default NewPostOfInterest;