import React from 'react';
import PropTypes from 'prop-types';

const Layout = ({header, children}) => {
    return (
        <table>
            <tr>
                <h1>{header}</h1>
            </tr>
            <tr>
                <td>{children}</td>
            </tr>
        </table>
    );
};

Layout.propTypes = {
    header: PropTypes.string.isRequired,
    children: PropTypes.object.isRequired,
};

export default Layout;