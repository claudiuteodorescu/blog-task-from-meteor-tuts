import React from 'react';
import PropTypes from 'prop-types';
import MainMenu from './MainMenu/MainMenu.jsx';

const App = ({main: component, routeProps}) => {
    /**
     * this is so that we can use the Router's `route` method
     * without a React component
     */
    if (!component) {
        return null;
    }

    return (
        <div id="app">
            <MainMenu />
            <div>
                {React.createElement(component, routeProps)}
            </div>
        </div>
    );
};

App.propTypes = {
    routeProps: PropTypes.object.isRequired
};

export default App;