import React from 'react';
import RouteService from '../../../services/routes/RouteService';
import MenuItemSeparator from './MenuItemSeparator';

export default () => {
    return (
        <span>
            <MenuItemSeparator />
            <a href={RouteService.newPostUrl()}>
                Create Post
            </a>
        </span>
    );
};