import React from 'react';
import RouteService from '../../../services/routes/RouteService';
import {Meteor} from 'meteor/meteor';
import Users from '../../../db/users/collection';
import {Accounts} from 'meteor/accounts-base';
import {withTracker} from 'meteor/react-meteor-data';
import RolesEnum from '/imports/core/db/users/enums/roles';
import Security from '/imports/core/security';
import LogoutMenuItem from './LogoutMenuItem';
import NewPostMenuItem from './NewPostMenuItem';
import PendingMenuItem from './PendingMenuItem';
import MenuItemSeparator from './MenuItemSeparator';

const MainMenu = ({currentUser}) => {
    const isAdmin = currentUser && Security.hasRole(currentUser, RolesEnum.ADMIN);

    return (
        <div className="blgt-main-menu">
            <a href={RouteService.postsUrl()}>Posts</a>

            {currentUser ? <NewPostMenuItem /> : null}

            {isAdmin ? <PendingMenuItem /> : null}

            <span className="blgt-main-menu__user-area">
                {
                    currentUser
                    ?
                    <div>
                        <a href={RouteService.userProfileUrl()}>My Profile</a>
                        <MenuItemSeparator />
                        <LogoutMenuItem />
                    </div>
                    :
                    <span>
                        <a href={RouteService.registerUrl()}>Register</a>
                        <MenuItemSeparator />
                        <a href={RouteService.loginUrl()}>Login</a>
                    </span>
                }
            </span>
        </div>
    );
}

// THIS SHOULD NOT BE HERE BUT WE DO THIS JUST THIS TIME BECAUSE IT'S
// HARD TO LOGIN FROM THE ADMIN APP to the APPLICATION APP
if (Meteor.settings.public.applicationName === 'app') {
    // Meteor.loginWithPassword('user1@example.com', 'pass');
    Meteor.loginWithPassword('admin1@example.com', 'pass');
}

export default withTracker(props => {
    return {
        currentUser: Meteor.user()
    };
})(MainMenu);