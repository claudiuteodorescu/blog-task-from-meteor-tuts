import React from 'react';
import {Meteor} from 'meteor/meteor';
import RouteService from '../../../services/routes/RouteService';

export default () => {
    const performLogout = (event) => {
        event.preventDefault();

        Meteor.logout((err) => {
            if (err) {
                return alert(err);
            }
        });

        window.location.href = RouteService.logoutUrl();
    };

    return <a href='' onClick={performLogout}>Logout</a>;
};