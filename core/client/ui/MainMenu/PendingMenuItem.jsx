import React from 'react';
import RouteService from '../../../services/routes/RouteService';
import MenuItemSeparator from './MenuItemSeparator';

export default () => (
    <span>
        <MenuItemSeparator />
        <a href={RouteService.pendingPostsUrl()}>
            Pending
        </a>
    </span>
);