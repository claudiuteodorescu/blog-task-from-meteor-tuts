import {createUsers, createUser, createAdmin} from './createUsers';
import {createPosts} from './createPosts';
import {Users} from '../db';

const runFixtures = () => {
    const shouldRun = Users.find().count() === 0;

    if (shouldRun) {
        console.log('Creating fixtures...');

        try {
            createUsers();

            createPosts();

            console.log("DONE!")
        }
        catch(err) {
            console.log("ERROR:", err);
        }
    }
};

if (Meteor.isDevelopment && !Meteor.isTest) {
    runFixtures();
}

export default runFixtures;

export {runFixtures, createUser, createAdmin};

