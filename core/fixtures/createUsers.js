import {Accounts} from 'meteor/accounts-base';
import {Roles} from 'meteor/alanning:roles';
import PostTagsEnum from '../db/posts/enums/tags';
import Config from './config';
import {Users} from '../db';

const createUsers = () => {
    _.range(Config.USERS_COUNT).map((userIndex) => {
        const addRandomInterests = true;
        return createUser(`user${userIndex}`, addRandomInterests);
    });

    _.range(Config.ADMINS_COUNT).map((adminIndex) => {
        return createAdmin(`admin${adminIndex}`);
    });
};

const createUser = (username, addRandomInterests = false) => {
    const userId = createBareUser(username);

    if (addRandomInterests) {
        const interests = pickRandomPostTags();
        Users.update(userId, {$set: {interests}});
    }

    return userId;
};

const createAdmin = (username) => {
    const adminId = createUser(username);

    Roles.addUsersToRoles(adminId, 'admin');

    return adminId;
};

const createBareUser = (username) => {
    const password = 'pass';
    const email = username + '@example.com';

    let options = {
        username,
        password,
        email,
        profile: {
            name: username.charAt(0).toUpperCase() + username.slice(1)
        }
    };

    if (user = Accounts.findUserByUsername(username)) {
        return user._id;
    }

    const userId = Accounts.createUser(options);
    return userId;
}

const pickRandomPostTags = () => {
    return _.sample(_.values(PostTagsEnum), _.random(1, _.size(PostTagsEnum)));
};

export default createUsers;

export {createUsers, createUser, createAdmin};