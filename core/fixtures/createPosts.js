import PostTagsEnum from '../db/posts/enums/tags';
import Config from './config';
import {Posts, Users} from '../db';

let usersCount = 0;
const createPosts = () => {
    usersCount = Users.find().count();

    _.range(Config.PENDING_POSTS_COUNT).map((postIndex) => {
        return createPost(postIndex);
    })

    _.range(Config.APPROVED_POSTS_COUNT).map((postIndex) => {
        return createPost(postIndex, {title: `Approved post ${postIndex}`, approved: true});
    })
}

const createPost = (postIndex, additionalPostData = {}) => {
    const postData = {
        title: `Post ${postIndex} title`,
        description: `post ${postIndex} description`,
        tags: _.sample(_.values(PostTagsEnum), _.random(1, _.size(PostTagsEnum))),
        ...additionalPostData
    }

    if (usersCount > 0) {
        const aUserId = Users.findOne({}, {fields: {_id: 1}})._id;
        postData.createdBy = aUserId;
    }

    return Posts.insert(postData);
}

export {createPosts};