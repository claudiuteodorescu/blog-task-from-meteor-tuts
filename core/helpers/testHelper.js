import sinon from 'sinon';
import {assert} from 'chai';
import {Users, Posts} from '../db';
import {createUser as createUserFixture, createAdmin} from '../fixtures';
import {resetDatabase} from 'meteor/xolvio:cleaner';
import {PostTagsEnum} from '/imports/core/db/posts/enums/tags';
import {Email} from 'meteor/email';

let userId, adminId;

before(function() {
    // don't send emails while testing
    sinon.stub(Email, 'send');
})

beforeEach(function(done) {
    resetDatabase(null, function() {
        userId = createUser('testUser');
        adminId = createAdmin('testAdmin');

        done();
    });
});

const createUser = function(username, userData) {
    const userId = createUserFixture(username);
    if (userData) {
        Users.update(userId, {$set: userData});
    }
    return userId;
};

const getPostDataSample = function() {
    return {
        title: 'Some title',
        description: 'A nice post description',
        tags: [PostTagsEnum.SOFTWARE, PostTagsEnum.SPORTS],
    };
};

const restoreIfStubbed = function(func) {
    if (func.restore) {
        func.restore();
    }
};

const createPost = function(additionalPostData = {}) {
    let postId;

    try {
        const postData = _.extend(getPostDataSample(), {createdBy: userId}, additionalPostData);
        postId = Posts.insert(postData);

        assert.typeOf(postId, 'string');
    }
    catch(e) {
        console.log("ERROR:", e);
    }

    return postId;
};

const createApprovedPost = function(postData) {
    const postId = createPost(postData);
    Posts.update(postId, {$set: {approved: true}});
    assert.isTrue(Posts.findOne(postId).approved);

    return postId;
};

const getUserEmail = function(userId) {
    const user = Users.findOne(userId);
    assert.isOk(user);

    return user.emails[0].address;
}

export {
    userId,
    adminId,
    getPostDataSample,
    getUserEmail,
    restoreIfStubbed,
    createUser,
    createPost,
    createApprovedPost,
    createAdmin,
    createApprovedPostForEmailTemplate,
};