import 'meteor/cultofcoders:grapher';
import Posts from './collection';
import Users from '../users/collection';

Posts.addLinks({
    author: {
        collection: Users,
        type: 'one',
        field: 'createdBy',
    }
});