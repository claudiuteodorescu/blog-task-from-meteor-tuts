import {Mongo} from 'meteor/mongo';

Posts = new Mongo.Collection('posts');

Posts.attachBehaviour('timestampable', {
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  createdBy: 'createdBy',
  updatedBy: 'updatedBy'
});

export default Posts;