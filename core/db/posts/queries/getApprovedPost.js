import Posts from '../collection';

export default Posts.createQuery('getApprovedPost', {
    $filter({filters, options, params}) {
        filters.approved = true;

        if (params.postId) {
            filters._id = params.postId;
        }
    },
    title: 1,
    description: 1,
    tags: 1,
    author: {
        profile: {
            name: 1
        }
    }
});