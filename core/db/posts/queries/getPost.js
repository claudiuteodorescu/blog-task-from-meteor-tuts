import Posts from '../collection';

export default Posts.createQuery('getPost', {
    title: 1,
    description: 1,
    tags: 1,
    approved: 1,
    author: {
        profile: {
            name: 1
        }
    }
});