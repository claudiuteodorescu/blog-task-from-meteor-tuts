import Posts from '../collection';

export default Posts.createQuery('getPendingPost', {
    $filter({filters, params}) {
        if (params.postId) {
            filters._id = params.postId;
        }

        filters.approved = {$exists: false};
    },
    title: 1,
    description: 1,
    tags: 1,
    author: {
        emails: {
            address: 1
        }
    }
});