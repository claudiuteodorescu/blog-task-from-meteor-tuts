const PostTagsEnum = {
    PSYCHOLOGY: 'psychology', 
    PHILOSOPHY: 'philosophy',
    SPORTS: 'sports',
    SOFTWARE: 'software',
    ROBOTICS: 'robotics',
};

const PostTagsLabels = {
    [PostTagsEnum.PSYCHOLOGY]: 'Psychology & Education',
    [PostTagsEnum.PHILOSOPHY]: 'Philosophy & Arts',
    [PostTagsEnum.SPORTS]: 'Sports',
    [PostTagsEnum.SOFTWARE]: 'Computer Software',
    [PostTagsEnum.ROBOTICS]: 'Robotics and Automation',
}

export default PostTagsEnum;
export {PostTagsEnum, PostTagsLabels};