import Users from '../collection';

export default Users.createQuery('getUser', {
    interests: 1,

    profile: {name: 1},
});