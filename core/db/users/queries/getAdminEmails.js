import Users from '../collection';
import RolesEnum from '../enums/roles';

export default Users.createQuery('getAdminEmails', {
    $filters: {roles: {$in: [RolesEnum.ADMIN]}},

    $options: {fields: {_id: 0}},

    emails: {address: 1},
});