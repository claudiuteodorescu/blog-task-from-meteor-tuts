import Users from '../collection';

// TODO: check security...
export default Users.createQuery('getUsersByInterests', {
    $filter({filters, params}) {
        if (params.interests && _.isArray(params.interests)) {
            filters.interests = {$in: params.interests};
        }
    },

    $options: {fields: {_id: 0}},

    emails: {address: 1},
});