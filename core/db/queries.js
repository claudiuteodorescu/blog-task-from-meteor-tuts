import getAdminEmailsQuery from './users/queries/getAdminEmails';
import getUsersByInterestsQuery from './users/queries/getUsersByInterests';
import getPendingPostQuery from './posts/queries/getPendingPost';
import getApprovedPostQuery from './posts/queries/getApprovedPost';
import getPostQuery from './posts/queries/getPost';

export {
    getAdminEmailsQuery,
    getPendingPostQuery,
    getUsersByInterestsQuery,
    getApprovedPostQuery,
    getPostQuery,
};