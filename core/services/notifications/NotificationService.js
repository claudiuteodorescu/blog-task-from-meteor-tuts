import Users from '../../db/users/collection';
import Posts from '../../db/posts/collection';
import ApprovePostRequest from '../../emails/templates/ApprovePostRequest.jsx';
import NewPostOfInterest from '../../emails/templates/NewPostOfInterest.jsx';
import EmailService from '../emails/EmailService';
import {
    getAdminEmailsQuery,
    getPendingPostQuery,
    getApprovedPostQuery,
    getUsersByInterestsQuery
} from '../../db/queries';

class NotificationService {
    static notifyAdminsForPostApproval(postId) {
        // TODO: admin emails is actually admins with email:
        // rename or refactor
        const adminEmails =
            getAdminEmailsQuery
            .clone()
            .fetch()
            .map(admin => admin.emails[0].address);
        
        const post =
            getPendingPostQuery
            .clone({postId})
            .fetchOne();

        EmailService.send({
            to: adminEmails,
            subject: 'Post Approval Request',
        }, ApprovePostRequest, {post});
    }

    static notifyUsersInterestedOfPost(postId) {
        const post = getApprovedPostQuery.clone({postId}).fetchOne();
        const postTags = post.tags || [];
        const interestedUsers = getUsersByInterestsQuery.clone({interests: postTags}).fetch();

        EmailService.send({
            to: interestedUsers.map(user => user.emails[0].address),
            subject: 'A new post of interest was created',
        }, NewPostOfInterest, {post});
    }
};

export default NotificationService;