import {assert} from 'chai';
import sinon from 'sinon';
import {resetDatabase} from 'meteor/xolvio:cleaner';
import {Users, Posts, PostTagsEnum} from '../../../../db';
import {Email} from 'meteor/email';
import {Emitter, EventsEnum} from '../../../../events';
import NotificationService from '../../../../services/notifications/NotificationService';
import EmailService from '../../../../services/emails/EmailService';
import ApprovePostRequest from '../../../../emails/templates/ApprovePostRequest.jsx';
import NewPostOfInterest from '../../../../emails/templates/NewPostOfInterest.jsx';
import {
    getUserEmail,
    createApprovedPost,
    createPost,
    createUser,
    createAdmin,
    getPostDataSample,
} from '../../../../helpers/testHelper';

describe('Notification Service', function(done) {
    beforeEach(function(done) {
        resetDatabase(null, done);
    });

    afterEach(function() {
        EmailService.send.restore();
    });

    it('Should notify admins when a pending post is approved', function() {
        const authorId = createUser('anotherTestUser');
        const postId = createPost({createdBy: authorId});
        const post = Posts.findOne(postId);

        sinon.stub(EmailService, 'send');

        const adminId1 = createAdmin('testAdmin1');
        const adminId2 = createAdmin('testAdmin2');
        const adminEmails = [getUserEmail(adminId1), getUserEmail(adminId2)];

        NotificationService.notifyAdminsForPostApproval(postId);

        const mailConfig = {
            to: adminEmails,
            subject: 'Post Approval Request'
        };
        
        const ReactComponent = ApprovePostRequest;

        const props = {post: {
            _id: postId,
            title: post.title,
            description: post.description,
            author: {_id: authorId, emails: [{address: getUserEmail(authorId)}]},
            tags: getPostDataSample().tags,
        }};

        assert.deepEqual(
            EmailService.send.getCall(0).args,
            [mailConfig, ReactComponent, props]
        );
    });

    it('Should notify users interested of a post', function() {
        const authorId = createUser('anotherTestUser');
        const authorName = Users.findOne(authorId, {fields: {"profile.name": 1}}).profile.name;

        const postTags = [PostTagsEnum.SPORTS, PostTagsEnum.SOFTWARE];
        const postId = createApprovedPost({
            tags: postTags,
            createdBy: authorId,
        });
        const post = Posts.findOne(postId);

        const interests1 = [PostTagsEnum.SPORTS];
        const interestedUser1 = createUser('interestedUser1', {interests: interests1});

        const interests3 = [PostTagsEnum.PSYCHOLOGY];
        const uninsterestedUser = createUser('uninterestedUser', {interests: interests3});

        const interests2 = [PostTagsEnum.SPORTS, PostTagsEnum.SOFTWARE, PostTagsEnum.ROBOTICS];
        const interestedUser2 = createUser('interestedUser2', {interests: interests2});

        const interestedUsersEmails = [getUserEmail(interestedUser1), getUserEmail(interestedUser2)];

        sinon.stub(EmailService, 'send');

        NotificationService.notifyUsersInterestedOfPost(postId);

        const mailConfig = {
            to: interestedUsersEmails,
            subject: 'A new post of interest was created'
        };
    
        const ReactComponent = NewPostOfInterest;
        const props = {post: {
            _id: postId,
            title: post.title,
            description: post.description,
            approved: true,
            author: {
                _id: authorId,
                profile: {name: authorName}
            },
            tags: postTags
        }};

        assert.deepEqual(
            EmailService.send.getCall(0).args,
            [mailConfig, ReactComponent, props]
        );
    });
});