import Config from './config';

export default class RouteService {
    static postsUrl() {
        return Config.hosts.app + this.postsPath();
    }

    static postsPath() {
        return Config.paths.posts;
    }

    static pendingPostsUrl() {
        return Config.hosts.app + this.pendingPostsPath();
    }

    static pendingPostsPath() {
        return Config.paths.pendingPosts;
    }

    static postUrl(postId) {
        return Config.hosts.app + this.postPath(postId);
    }

    static postPath(postId) {
        return this.postsPath() + '/' + postId;
    }

    static newPostUrl() {
        return Config.hosts.app + this.newPostPath();
    }

    static newPostPath() {
        return Config.paths.newPost;
    }

    static userProfileUrl() {
        return Config.hosts.app + this.userProfilePath();
    }

    static userProfilePath() {
        return Config.paths.userProfile;
    }

    static loginUrl() {
        return Config.hosts.admin + this.loginPath();
    }

    static loginPath() {
        return Config.paths.login;
    }

    static logoutUrl() {
        return Config.hosts.admin + this.logoutPath();
    }

    static logoutPath() {
        return Config.paths.logout;
    }

    static registerUrl() {
        return Config.hosts.admin + this.registerPath();
    }

    static registerPath() {
        return Config.paths.register;
    }

    static forgotPasswordUrl() {
        return Config.hosts.admin + this.forgotPasswordPath();
    }

    static forgotPasswordPath() {
        return Config.paths.forgotPassword;
    }

    static resetPasswordUrl(token) {
        return Config.hosts.admin + this.resetPasswordPath(token);
    }

    static resetPasswordPath(token) {
        return Config.paths.resetPassword + '/' + token;
    }

    static adminHost() {
        return Config.hosts.admin;
    }
}