// TODO: rename this file to routes.config.js

export default {
    hosts: {
        admin: 'http://localhost:3050',
        app: 'http://localhost:3000',
    },

    paths: {
        posts: '/posts',
        pendingPosts: '/pending',
        newPost: '/posts/new',
        login: '/authentication/login',
        logout: '/authentication/logout',
        register: '/authentication/register',
        forgotPassword: '/authentication/forgot-password',
        resetPassword: '/authentication/reset-password',
        userProfile: '/profile',
    }
}