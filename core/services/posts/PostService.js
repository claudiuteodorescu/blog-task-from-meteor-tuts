import Posts from '../../db/posts/collection';
import {Emitter, EventsEnum} from '../../events';

export default class PostService {
    static createPost(createdBy, {title, description, tags}) {
        const postId = Posts.insert({title, description, tags, createdBy});

        Emitter.emit(EventsEnum.POST_CREATED, {postId});

        return postId;
    }

    static approvePost(postId) {
        const updatedPostsCount = Posts.update(
            {_id: postId, approved: {$exists: false}},
            {$set: {approved: true}}
        );

        const approved = updatedPostsCount > 0;

        if (approved) {
            Emitter.emit(EventsEnum.POST_APPROVED, {postId});
        }

        return approved;
    }
};