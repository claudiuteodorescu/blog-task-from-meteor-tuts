import {assert} from 'chai';
import sinon from 'sinon';
import {Mongo} from 'meteor/mongo';
import PostService from '../../../posts/PostService';
import {Posts} from '../../../../db';
import {Emitter, EventsEnum} from '../../../../events';
import {userId, getPostDataSample, createApprovedPost, createPost} from '../../../../helpers/testHelper';

describe('Post Service', function() {
    describe('Create', function() {
        it('Should create a post', function() {
            const postId = PostService.createPost(userId, getPostDataSample());
            const post = Posts.findOne(postId);
            
            assert.deepInclude(post, {
                _id: postId,
                title: getPostDataSample().title,
                description: getPostDataSample().description,
                createdBy: userId,
                tags: getPostDataSample().tags,
            });
        });

        it('Should return the id of the created post', function() {
            const postId = PostService.createPost(userId, getPostDataSample());
            assert.typeOf(postId, 'string');
        });

        it('Should trigger a `post created` event when post created', function() {
            sinon.stub(Emitter, 'emit');

            const postId = PostService.createPost(userId, getPostDataSample());

            assert.isTrue(Emitter.emit.calledWith(EventsEnum.POST_CREATED, {postId}));

            Emitter.emit.restore();
        });
    });

    describe('Approve', function() {
        let pendingPostId;

        beforeEach(function() {
            pendingPostId = createPost();
        });

        it('Should approve a post that has not yet been approved', function() {
            PostService.approvePost(pendingPostId);
            assert.isTrue(Posts.findOne(pendingPostId).approved);
        });

        it('Should return true when has approved a post', function() {
            const approved = PostService.approvePost(pendingPostId);
            assert.isTrue(approved);
        });

        it('Should not disapprove a post that is already approved', function() {
            const approvedPostId = createApprovedPost(userId);

            PostService.approvePost(approvedPostId);
            assert.isTrue(Posts.findOne(approvedPostId).approved);
        });

        it('Should return false if the post does not exist', function() {
            assert.isFalse(PostService.approvePost('inexistentPostId'));
        });

        it('Should trigger a `post approved` event when post approved', function() {
            sinon.stub(Emitter, 'emit');

            const approved = PostService.approvePost(pendingPostId);
            assert.isTrue(approved);

            assert.isTrue(Emitter.emit.calledWith(EventsEnum.POST_APPROVED, {postId: pendingPostId}));

            Emitter.emit.restore();
        });

        it('Should not trigger the `post approved` event if post does not exist', function() {
            sinon.stub(Emitter, 'emit');

            PostService.approvePost('inexistentPostId');

            assert.isTrue(Emitter.emit.notCalled);

            Emitter.emit.restore();
        });

        it('Should not trigger the `post approved` event if post is already approved', function() {
            sinon.stub(Emitter, 'emit');

            const approvedPostId = createApprovedPost(userId);
            PostService.approvePost(approvedPostId);

            assert.isTrue(Emitter.emit.notCalled);

            Emitter.emit.restore();
        });
    });
});