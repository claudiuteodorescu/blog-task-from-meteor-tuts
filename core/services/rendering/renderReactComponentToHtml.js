import React from 'react';
import ReactDOM from 'react-dom/server';

export default function(Component, props) {
    const element = React.createElement(Component, props);
    return ReactDOM.renderToString(element);
};