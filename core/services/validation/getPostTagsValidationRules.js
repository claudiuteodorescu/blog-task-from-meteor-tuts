import {PostTagsEnum} from '../../db/posts/enums/tags';

export default function(field = 'tags') {
    return {
        [field]: {
            type: Array,
            optional: true,
        },
        [`${field}.$`]: {
            type: String,
            allowedValues: _.values(PostTagsEnum),
            optional: true,
        }
    };
}