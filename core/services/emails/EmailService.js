import renderReactComponentToHtml from '/imports/core/services/rendering/renderReactComponentToHtml';
import {Email} from 'meteor/email';

const production = Meteor.isDevelopment;

const FROM = Meteor.settings.emails.from;

/**
 * React Email Sender
 */
class EmailService {
    static send(mainConfig, Component, props) {
        let options = _.extend({
            from: FROM,
            html: renderReactComponentToHtml(Component, props)
        }, mainConfig);

        Email.send(options);
    }
}

export default EmailService;    